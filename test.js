const Output = require('./main')

var out = new Output("Downloading, %%s", [80], {
    timestamp: false
})
out.out()

out = new Output("Download complete");
out.out()

out = new Output("Downloading %s%", [10])
out.out()