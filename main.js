/**
 * @var {string} pattern
 * @var {array}  variables
 * @var {object} options
 *
 * @return {any} void
 */
module.exports = function (pattern, variables, options) {

    this.pattern = ''
    this.variables = []
    this.options = { timestamp: true }

    //beginConstruction
    this.pattern = pattern

    if (typeof variables != 'undefined') {
        this.variables = variables
    }

    if (typeof options != 'undefined') {
        this.options = options
    }
    ///endConstruction

    this.out = () => {
        let out = ''

        let pattern = String(this.pattern).split('%s')

        Object.values(pattern).forEach(((value, index) => {

            if (typeof this.variables[index] == 'undefined') {
                this.variables[index] = ''
            }

            out += value + this.variables[index]

        }).bind(this));

        if (out.length > 0) {
            if (this.options.timestamp == true) {
                require('log-timestamp')
            }

            console.log(out)
        }
    }

}