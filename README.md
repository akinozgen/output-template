# NodeJS output template management package

Usage:
```javascript
const Output = require('./main')

var out = new Output("Downloading, %%s", [80], {
    timestamp: false
})
out.out()
>>> "Downloading, %80"

out = new Output("Download complete");
out.out()
>>> "[2017-05-23T16:47:19.560Z] Download complete"

out = new Output("Downloading %s%", [10])
out.out()
>>> "[2017-05-23T16:47:19.562Z] Downloading 10%"
```